<?php
/**
 * @author      Jonathan Barros <jhow_monteiro@yahoo.com.br>
 * @copyright   2021 Jonathan
 * @license     MIT  Copyright
 *
 * @link        https://opensource.org/licenses/MIT
 */
declare(strict_types=1);

namespace J2M\Configurator\Test\Unit\Console;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Xml\Parser;
use J2M\Configurator\Console\Configurator;
use J2M\Configurator\Helper\PhpUnit;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConfiguratorTest - test of Configurator Class
 */
class ConfiguratorTest extends TestCase
{
    /**
     * @var MockObject $configInterfaceMock
     */
    protected $configInterfaceMock;

    /**
     * @var MockObject $readerMock
     */
    protected $readerMock;

    /**
     * @var MockObject $parserMock
     */
    protected $parserMock;

    /**
     * @var MockObject $inputMock
     */
    protected $inputMock;

    /**
     * @var MockObject $outputMock
     */
    protected $outputMock;

    /**
     * @var Configurator $model
     */
    protected Configurator $model;

    /**
     * @inheridoc
     */
    public function setUp(): void
    {
        $this->inputMock = $this->getInputMock();
        $this->outputMock = $this->getOutputMock();
        $this->readerMock = $this->getReaderMock();
        $this->parserMock = $this->getParserMock();
        $this->configInterfaceMock = $this->getConfigInterfaceMock();

        $this->model = new Configurator(
            $this->configInterfaceMock,
            $this->readerMock,
            $this->parserMock
        );
    }

    /**
     * Method to mock the input interface
     *
     * @return MockObject
     */
    private function getInputMock(): MockObject
    {
        return $this->getMockBuilder(InputInterface::class)
            ->onlyMethods(['getOption'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass()
            ;
    }

    /**
     * Method to mock the output interface
     *
     * @return MockObject
     */
    private function getOutputMock(): MockObject
    {
        return $this->getMockBuilder(OutputInterface::class)
            ->onlyMethods(['writeln'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass()
            ;
    }

    /**
     * Method to mock the Reader class
     *
     * @return MockObject
     */
    private function getReaderMock(): MockObject
    {
        return $this->getMockBuilder(Reader::class)
            ->onlyMethods(['getModuleDir'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass()
            ;
    }

    /**
     * Method to mock the Parser class
     *
     * @return MockObject
     */
    private function getParserMock(): MockObject
    {
        return $this->getMockBuilder(Parser::class)
            ->onlyMethods(['load', 'xmlToArray'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass()
            ;
    }

    /**
     * Method to mock the ConfigInterface class
     *
     * @return MockObject
     */
    private function getConfigInterfaceMock(): MockObject
    {
        return $this->getMockBuilder(ConfigInterface::class)
            ->onlyMethods(['saveConfig'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass()
            ;
    }

    /**
     * Test the method execute with empty configs
     *
     * @throws \ReflectionException
     */
    public function testExecuteEmptyConfigs(): void
    {
        $pathXml = 'app/code/J2M/Configurator/etc/' . Configurator::XML_NAME;
        $xmlArray = [
            'config' => [
                '_value' => [
                    'configurations' => ''
                ]
            ]
        ];

        $this->readerMock->expects($this->once())->method('getModuleDir')
            ->with('etc', 'J2M_Configurator')->willReturn($pathXml);

        $this->parserMock->expects($this->once())->method('load')->willReturnSelf();
        $this->parserMock->expects($this->once())->method('xmlToArray')->willReturn($xmlArray);

        PhpUnit::callInaccessibleMethod($this->model, 'execute', [$this->inputMock, $this->outputMock]);
    }

    /**
     * Test the method execute with one config
     *
     * @throws \ReflectionException
     */
    public function testExecuteOneConfig(): void
    {
        $pathXml = 'app/code/J2M/Configurator/etc/' . Configurator::XML_NAME;
        $xmlArray = [
            'config' => [
                '_value' => [
                    'configurations' => [
                        'config' => [
                            '_value' => "1",
                            '_attribute' => [
                                'path' => 'web/cookie/cookie_restriction'
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->readerMock->expects($this->once())->method('getModuleDir')
            ->with('etc', 'J2M_Configurator')->willReturn($pathXml);

        $this->parserMock->expects($this->once())->method('load')->willReturnSelf();
        $this->parserMock->expects($this->once())->method('xmlToArray')->willReturn($xmlArray);
        $this->configInterfaceMock->expects($this->once())->method('saveConfig');
        // @codingStandardsIgnoreStart
        $this->outputMock->expects($this->once())->method('writeln')->with(
            "The config web/cookie/cookie_restriction value has been changed to 1 on the Scope default and the scopeId 0"
        );
        // @codingStandardsIgnoreEnd
        PhpUnit::callInaccessibleMethod($this->model, 'execute', [$this->inputMock, $this->outputMock]);
    }

    /**
     * Test the method execute with one config
     *
     * @throws \ReflectionException
     */
    public function testExecuteTwoConfigs(): void
    {
        $pathXml = 'app/code/J2M/Configurator/etc/' . Configurator::XML_NAME;
        $xmlArray = [
            'config' => [
                '_value' => [
                    'configurations' => [
                        'config' => [
                            [
                                '_value' => "1",
                                '_attribute' => [
                                    'path' => 'web/cookie/cookie_restriction'
                                ]
                            ],
                            [
                                '_value' => "2",
                                '_attribute' => [
                                    'path' => 'web/cookie/cookie_restrictions',
                                    'scope' => 'website',
                                    'scopeId' => "2"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->readerMock->expects($this->once())->method('getModuleDir')
            ->with('etc', 'J2M_Configurator')->willReturn($pathXml);

        $this->parserMock->expects($this->once())->method('load')->willReturnSelf();
        $this->parserMock->expects($this->once())->method('xmlToArray')->willReturn($xmlArray);
        $this->configInterfaceMock->expects($this->atLeast(2))->method('saveConfig');
        $this->outputMock->expects($this->atLeast(2))->method('writeln');

        PhpUnit::callInaccessibleMethod($this->model, 'execute', [$this->inputMock, $this->outputMock]);
    }

    /**
     * Method to test the set config returning false
     */
    public function testSetConfigException(): void
    {
        $path = 'web/cookie/cookie_restriction';
        $value = 1;
        $scope = 'default';
        $scopeId = 0;

        $this->configInterfaceMock->expects($this->once())->method('saveConfig')
            ->will($this->throwException(new \Exception()));

        $this->outputMock->expects($this->once())->method('writeln')->with(
            "Error to set config {$path}. Value = {$value}, Scope = {$scope} and the scopeId = {$scopeId}"
        );

        PhpUnit::callInaccessibleMethod($this->model, 'setConfig', [$this->outputMock, $path, $value]);
    }
}
