<?php
/**
 * @author      Jonathan Barros <jhow_monteiro@yahoo.com.br>
 * @copyright   2021 Jonathan
 * @license     MIT  Copyright
 *
 * @link        https://opensource.org/licenses/MIT
 */
declare(strict_types=1);

namespace J2M\Configurator\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class PhpUnit - Class to call inaccessible methods
 *
 * @codeCoverageIgnore
 */
class PhpUnit extends AbstractHelper
{
    // @codingStandardsIgnoreStart
    /**
     * Method to access private and protected functions
     *
     * @param $obj
     * @param string $methodName
     * @param array $args
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public static function callInaccessibleMethod($obj, string $methodName, array $args)
    {
        $class = new \ReflectionClass($obj);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($obj, $args);
    }
    // @codingStandardsIgnoreEnd
}
