<?php
/**
 * @author      Jonathan Barros <jhow_monteiro@yahoo.com.br>
 * @copyright   2021 Jonathan
 * @license     MIT  Copyright
 *
 * @link        https://opensource.org/licenses/MIT
 */
declare(strict_types=1);

namespace J2M\Configurator\Console;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Xml\Parser;
use Magento\Store\Model\Store;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Configurator - Class to create configurator command
 */
class Configurator extends Command
{
    const XML_NAME = 'configurator.xml';

    /**
     * @var ConfigInterface $configInterface
     */
    protected ConfigInterface $configInterface;

    /**
     * @var Reader $moduleDirReader
     */
    protected Reader $moduleDirReader;

    /**
     * @var Parser $parser
     */
    private Parser $parser;

    /**
     * Configurator constructor.
     *
     * @param ConfigInterface $configInterface
     * @param Reader $moduleDirReader
     * @param Parser $parser
     * @param string|null $name
     */
    public function __construct(
        ConfigInterface $configInterface,
        Reader $moduleDirReader,
        Parser $parser,
        string $name = null
    ) {
        $this->configInterface = $configInterface;
        $this->moduleDirReader = $moduleDirReader;
        $this->parser = $parser;

        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName('j2m:configurator');
        $this->setDescription('Insert the config values on configurator xml to database');

        parent::configure();
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $configs = $this->getConfigs();
        if ($configs && !empty($configs['config'])) {
            if (empty($configs['config'][0]) && isset($configs['config']['_value'])) {
                $configs['config'] = [$configs['config']];
            }

            foreach ($configs['config'] as $config) {
                $this->setConfig(
                    $output,
                    $config['_attribute']['path'],
                    $config['_value'],
                    isset($config['_attribute']['scope']) ? $config['_attribute']['scope'] :
                        ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                    isset($config['_attribute']['scopeId']) ? (int)$config['_attribute']['scopeId']:
                        Store::DEFAULT_STORE_ID
                );
            }
        }
    }

    /**
     * Set the config
     *
     * @param OutputInterface $output
     * @param string $path
     * @param string $value
     * @param string $scope
     * @param int $scopeId
     *
     * @return bool
     */
    protected function setConfig(
        OutputInterface $output,
        string $path,
        string $value,
        string $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        int $scopeId = Store::DEFAULT_STORE_ID
    ): bool {
        try {
            $this->configInterface->saveConfig($path, $value, $scope, $scopeId);
            $output->writeln(
                "The config {$path} value has been changed to {$value} on the Scope {$scope} and the scopeId {$scopeId}"
            );
            return true;
        } catch (\Exception $e) {
            $output->writeln(
                "Error to set config {$path}. Value = {$value}, Scope = {$scope} and the scopeId = {$scopeId}"
            );
            return false;
        }
    }

    /**
     * Get the configs from configurator.xml
     *
     * @return array
     */
    protected function getConfigs(): array
    {
        $filePath = $this->moduleDirReader->getModuleDir(
            'etc',
            'J2M_Configurator'
        ) . '/' . self::XML_NAME;

        $parsedArray = $this->parser->load($filePath)->xmlToArray();

        return (array)$parsedArray['config']['_value']['configurations'];
    }
}
